#!/usr/bin/env python3

import json

kind = input('what is the kind of you wanted? ')
kinds = ('c', 's')
assert kind in kinds

confc = {
}

# server need to handle [TLS to TCP plaintext] itself.
confs = {
  "inbounds": [{
    "listen": "127.0.0.1",
    "port": 8800,
    "protocol": "vmess",
    "settings": {
      "clients": [
        {
          "id": "00000000-0000-0000-0000-000000000000",
          "alterId": 0
        }
      ]
    },
    "streamSettings": {
        "network": "ws",
        "wsSettings": {
            "path": "/"
        },

        "security": "none"
    }
  }],
  "outbounds": [{
    "protocol": "freedom",
    "settings": {}
  }],
}

if kind == 's':
    port = int(input("type the server's local websocket listen port: "))
    assert port >= 0 and port <= 65535
    confs['inbounds'][0]['port'] = port

    confs = json.dumps(confs, indent=2)
    print(confs)

elif kind == 'c':
    print('client config generate is not supported now')
    raise SystemExit
else:
    raise


