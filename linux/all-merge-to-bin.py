#!/usr/bin/env python3

print('== This program is used to merge all other binary directories into /usr/bin ==')
print('== Version: 1.2-beta ==')

import sys
import os
import shutil

if sys.platform != 'linux':
    print('[ERROR] This program is only available for Linux, any other systems is not supported.')
    sys.exit()

if os.getuid() != 0:
    print("[WARN] This program requires root access, please run this program as root.")
    sys.exit()


if os.path.islink('/usr/bin'):
    print("Your system's /usr/bin directory is linked to another directory, please make sure it is a regular directory.")
    sys.exit()

cp = f'/tmp/cp-{os.urandom(32).hex()}'
assert os.system(f'cp /usr/bin/cp {cp}') == 0

def is_merged(path):
    assert type(path) is str
    assert os.path.isabs(path)

    if path.endswith('/'):
        path = path[:-1]

    if os.path.isdir(path) and os.path.islink(path) and os.path.abspath(os.readlink(path)) == '/usr/bin':
        return True

    return False

def merge(path):
    assert type(path) is str
    assert os.path.isabs(path)

    if is_merged(path):
        print(f'[INFO] {path} has already been merged, so there is no need to merge it again.')
        return

    if os.path.abspath(path) == '/usr/bin':
        return

    if os.path.islink(path):
        print(f'[WARN] source directory {path} is a symlink, ignored.')
        return

    walk = os.walk(path)
    files = {}

    for it in walk:
        ls = it[0]
        for f in it[2]:
            af = ls + '/' + f
            if os.path.isfile(af):
                files[af] = f

    for af in files.keys():
        f = files[af]

        data = open(af, 'rb').read()

        to = '/usr/bin/' + f

        assert ' ' not in af
        assert ' ' not in to

        if os.path.exists(to):
            if os.path.islink(to):
                if os.readlink(to) == af:
                    os.unlink(to)

                    assert os.system(f'{cp} {af} {to}') == 0
                    continue
            elif os.path.isdir(to):
                raise IsADirectoryError(f'cannot merge {af} to /usr/bin , because target directory have a same name directory.')
            #else:
            #    raise Exception(f'cannot merge {af} to /usr/bin , because target directory have a same name thing.')

            if data == open(to, 'rb').read():
                continue
            
            raise FileExistsError(f'cannot merge {af} to /usr/bin , because the file is duplicated in the target directory /usr/bin')

        #open(to, 'wb').write(data)
        assert os.system(f'{cp} {af} {to}') == 0


    if os.path.islink(path):
        os.unlink(path)
    else:
        shutil.rmtree(path)

    if os.path.exists(path):
        raise FileExistsError('cannot create a symlink to /usr/bin , because the source directory is cannot removed.')

    os.symlink('/usr/bin/', path)


OTHER_BIN_DIR = ('/usr/local/sbin', '/usr/local/bin', '/usr/sbin', '/bin', '/sbin', '/usr/local/games', '/usr/games')

def main():
    input('press enter key to continue...')

    for it in OTHER_BIN_DIR:
        merge(it)

if __name__ == '__main__':
    main()

