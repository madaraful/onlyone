from telethon import TelegramClient, events
import requests_async
import json
import os
import time
import urllib.parse

requests_session = requests_async.Session()

translate_cache = {}
async def translate(From, To, Text):
    Text = urllib.parse.quote(Text)
    To = urllib.parse.quote(To)
    From = urllib.parse.quote(From)

    if (From, To, Text) in translate_cache.keys():
        res = translate_cache[(From, To, Text)]
        return res

    d = await requests_session.get(f'https://translate.google.com/translate_a/single?client=gtx&dt=t&dj=1&ie=UTF-8&sl={From}&tl={To}&q={Text}')
    
    res = json.loads(d.text)['sentences'][0]['trans']
    translate_cache[(From, To, Text)] = res
    return res

ip_query_cache = {}
async def ip_query(ip):
    ip = urllib.parse.quote(ip)

    if ip in ip_query_cache.keys():
        res, t = ip_query_cache[ip]
        if (time.time() - t) < 86400:
            return res

        del ip_query_cache[ip]

    d = await requests_session.get(f'https://ipapi.co/{ip}/json',
            headers={
                'user-agent': os.urandom(32).hex()
            })

    res = json.loads(d.text)
    ip_query_cache[ip] = (res, time.time())

    return res

api_id = 1054981
api_hash = '341e29114e1bb38d1fda9f1a22b59b28'
with TelegramClient('tgmadarafulbot', api_id, api_hash) as client:

    @client.on(events.NewMessage())
    async def new_message_handle(event):
        await ltran(event)
        await lip(event)

    async def lip(event):
        text = event.raw_text
        if text == '/ip':
            await event.reply('To use ip query function, please use command `/ip <ipv4/ipv6 address>`')
            return

        if not text.startswith('/ip'):
            return

        arg = text.split(' ')
        if len(arg) != 2:
            return

        ip = arg[1]

        res = await ip_query(ip)
        res = json.dumps(res, indent=2)
        res = '`'+res+'`'
        await event.reply(res)

    async def ltran(event):
        text = event.raw_text

        if text == '/tran':
            await event.reply("to use this bot's translation function, you can reply a message with command `/tran <source language> <destination language>`\n\nfor example: `/tran en ja`")
            return

        if not text.startswith('/tran '):
            return
        
        req = text.split(' ')
        req.pop(0)

        if len(req) != 2:
            return

        src = req[0]
        dst = req[1]

        orig = await event.get_reply_message()
        res = await translate(src, dst, orig.raw_text)
        #print(event.message.__dict__)
        #event.get
        await orig.reply(res)

    print('bot started')
    client.run_until_disconnected()

