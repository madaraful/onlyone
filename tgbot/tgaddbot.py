import sys
import time
import asyncio

from telethon.sync import TelegramClient, events
from telethon.tl.functions.channels import InviteToChannelRequest

GROUPS = [ 'tgInternetSecurity', 'notmanaged' ]
USERS = []
ED = []

api_id = 1054981
api_hash = '341e29114e1bb38d1fda9f1a22b59b28'
phone = sys.argv[1]

client = TelegramClient(phone, api_id, api_hash)

client.connect()
if not client.is_user_authorized():
    client.send_code_request(phone)
    client.sign_in(phone, input('telegram account login code: '))

@client.on(events.NewMessage)
async def new_msg_handler(event):
    global USERS

    user = await event.get_input_sender()
    user_id = user.user_id

    if user_id in ED:
        return

    print(user)
    USERS.append(user)

async def add_user_to_group():
    global GROUPS
    global USERS
    global ED

    try:
        user = USERS.pop(0)
    except IndexError:
        return

    try:
        user_id = user.user_id
        if user_id in ED:
            return

        for group in GROUPS:
            try:
                group = await client.get_entity(group)
            except BaseException as err:
                print(group, '|', repr(err))
                continue

            try:
                result = await client(InviteToChannelRequest(group, [user]))
            #except UserPrivacyRestrictedError as err:
            #    break
            except BaseException as err:
                print(user, '|', repr(err))
            else:
                print(f'Added user {user} to group {group.title}')
        
            time.sleep(1)
    except BaseException as err:
        #USERS.append(user)
        raise err

    ED.append(user_id)

client.start()

def main():
    loop = asyncio.get_event_loop()

    async def add():
        while True:
            try:
                await add_user_to_group()
            except BaseException as err:
                print(repr(err))

            await asyncio.sleep(10)
    
    loop.create_task(add())

    client.run_until_disconnected()

main()
